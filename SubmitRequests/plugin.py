###
# Copyright (c) 2016, Fire Bird
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###
#from lxml import etree
import sys
import urllib2
import unicodedata
import os
#import xml.dom.minidom
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

#Supybot imports
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('SubmitRequests')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

class SubmitRequests(callbacks.Plugin):
    """A plugin to obtain submit request information for openSUSE"""
    threaded = True

    def __mrspec(self, xmltree):
        root = xmltree.getroot()
        destdict = dict()
        reltag = 'releaseproject' # This changes to 'project' for
                                  # maintenance_release (tested later)
        reqcr = root.get('creator')

        # MR's have multiple <action> nodes, one for each release target
        # (prj/pkg). All nodes have the same acttype, but different source
        # and target subelements.
        # Iterate over every node to identify list of src and target pkgs/prjs
        for el in xmltree.iter('action'):
            acttype = el.get('type')
            src  = el.find("source")
            if src.get('package') != 'patchinfo':
                pkgname = src.get('package')
                dest   = el.find("target")

                if acttype == 'maintenance_release':
                    reltag = 'project'
                    pkgname = dest.get('package')
                    # For release reqs, the source project is usually of the
                    # form openSUSE:Maintenance:XXXX where X is a digit
                    # We split out XXXX for use with target pkgnames which are
                    # typically of the form "pkgname.XXXX" as well, so we
                    # remove the rightmost ".XXXX" to fix pkgname
                    mintprjid = src.get('project').rsplit(':')[-1]
                    pkgname = pkgname.replace('.' + mintprjid, '', 1)

                relprj = dest.get(reltag)

                if relprj not in destdict:
                    destdict[relprj] = [pkgname]
                else:
                    if pkgname not in destdict[relprj]:
                        destdict[relprj].append(pkgname)

        mess = '{:s} by {:s}; '.format(acttype, reqcr)

        # Do maint_release/incident have the same pkgs for each releaseproject?
        # Assume yes for now, i.e. use the same list "pkgs" for all target prj
        pkgs = destdict.values()[0]
        # Case 1: just the one pkg
        if len(pkgs) == 1:
            mess += '{:s} '.format(pkgs[0])

        # Case 2: just the two pkgs
        elif len(pkgs) == 2:
            mess += '{:s}, {:s} '.format(pkgs[0], pkgs[1])

        # Case 3: multiple (>2) pkgs
        else:
            mess += '{:d} packages ({:s}, {:s}...) '.format(len(pkgs),
                                                            pkgs[0],
                                                            pkgs[1])

        mess += 'for '
        for pr in destdict.keys():
            mess += '{:s}, '.format(pr)

        # Strip the final ", " from message...
        mess = mess[:-2]
        # ...and voila!
        return mess

    def sr(self, irc, msg, args, srid):
        """
        Display information about a Submit Request.
        """
        # Get sr# as user input
#        if (len(sys.argv) != 2):
#            irc.reply(r'Expecting sr# for input')
#            sys.exit(-1)

#        srnum=0
#        try:
#            srnum = int(sys.argv[1])
#        except ValueError:
#            irc.reply('Cannot convert input to integer')
#            sys.exit(-2)

        url  = 'https://api.opensuse.org/public/request/{:s}.xml'.format(srid)
        try:
            tree = ET.parse(urllib2.urlopen(url))
        except:
            irc.reply('Error while fetching info about sr#{0:s}. This could be '
                'because api.opensuse.org is down or if #{0:s} '
                'is an invalid id.'.format(srid))
            sys.exit(-1)

        root = tree.getroot()

        des  = root.find('description')

        destxt = ''
        if des == None or des.text == None:
            destxt = 'No description text'
        else:
            destxt = des.text
            destxt = destxt.replace('\n', ' ')
            destxt = destxt.replace('\r', ' ')

            ## TRIM destxt to 75 characters
            if (len(destxt) > 75):
                destxt = destxt[:74]+'...'

        act  = root.find('action')

        acttype = act.get('type')

        dest = act.find('target')
        destprj = dest.get('project')
        destpkg = dest.get('package')

        state  = root.find('state').get('name')
        if state == 'superseded':
            state += ' by {:s}'.format(root.find('state').get('superseded_by'))

        if acttype == 'submit':
            src    = act.find('source')
            srcprj = src.get('project')
            srcpkg = src.get('package')
            mess   = ('sr#{:s} [{:s} {:s}/{:s} -> {:s}/{:s}] {:s}'
                    ' (State: {:s})'.format(srid, acttype, srcprj, srcpkg, destprj,
                                            destpkg, destxt, state))

        elif acttype == 'maintenance_incident' or acttype == 'maintenance_release':
            mess   = ('sr#{:s} [{:s}] {:s}'
                      ' (State: {:s})'.format(srid, self.__mrspec(tree),
                                              destxt, state)
                     )


        elif acttype == 'delete':
            src  = ''
            srcprj = ''
            srcpkg = ''
            # Del req may be for entire projects instead of project/package
            # In these cases destprj is set to None, filter out accordingly
            if destpkg == None:
                mess = ('sr#{:s} [{:s} {:s}] {:s}'
                        ' (State: {:s})'.format(srid, acttype, destprj, destxt, state))
            else:
                mess = ('sr#{:s} [{:s} {:s}/{:s}] {:s} (State: {:s})'.format(srid, acttype,
                        destprj, destpkg, destxt, state))

        elif acttype == 'add_role':
            src  = ''
            srcprj = ''
            srcpkg = ''

            # add_role references the person and role requested
            per = act.find('person')
            name = per.get('name')
            role = per.get('role')
            # Del req may be for entire projects instead of project/package
            # In these cases destprj is set to None, filter out accordingly
            if destpkg == None:
                mess = ('sr#{:s} [{:s} {:s} as {:s} to {:s}] {:s}'
                        ' (State: {:s})'.format(srid, acttype, name, role,
                                                destprj, destxt, state))
            else:
                mess = ('sr#{:s} [{:s} {:s} as {:s} to {:s}/{:s}]'
                        '{:s} (State: {:s})'.format(srid, acttype, name, role,
                                                    destprj, destpkg, destxt, state))
        elif acttype == 'change_devel':
            # For change_devel requests, the source and target prj/pkg are reversed apprently!
            src    = act.find('source')
            srcprj = src.get('project')
            srcpkg = src.get('package')
            mess   = ('sr#{:s} [{:s} {:s}/{:s} -> {:s}/{:s}] {:s}'
                    ' (State: {:s})'.format(srid, acttype, destprj, destpkg, srcprj,
                                            srcpkg, destxt, state))

        else:
            mess = ('Sorry, I don\'t understand an sr with '
                    '\'action_type\' {:s} (yet!).'.format(acttype))


        irc.reply(mess + ' -- https://build.opensuse.org/request/show/{:s}'.format(srid))

    sr = wrap(sr, (['somethingWithoutSpaces']))

Class = SubmitRequests

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
