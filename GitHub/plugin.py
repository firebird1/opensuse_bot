#!/usr/bin/env python
# vim: set ai et ts=4 sw=4 tw=80:
###
# Copyright (c) 2016, Fire Bird and Atri Bhattacharya <badshah400@opensuse.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###
import sys
import requests as rq
import json
import unicodedata
import os

#Supybot imports
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
#try:
#    from supybot.i18n import PluginInternationalization
#    _ = PluginInternationalization('GitHub')
#except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
#    _ = lambda x: x

class GitHub(callbacks.Plugin):
#class GitHub():
    """A plugin to obtain github issue/pull requests"""
    threaded = True

    def gh(self, irc, msg, args, issuestr):
        """
        Display information about a Github issue/pull request.
        """
        # Get gh# as user input
#        if (len(sys.argv) != 2):
#            irc.reply(r'Expecting gh# for input')
#            sys.exit(-1)

        strs     = issuestr.split('#')
        if len(strs) != 3:
            irc.reply('Error parsing GH issue id string, should be gh#user/repo#xxx')
            sys.exit(-2)

        try:
            issueid = int(strs[2])
        except:
            irc.reply('Seriously? Issue ID must be a number, don\'t cha know!')
            sys.exit(-3)

        url = 'https://api.github.com/repos/{0:s}/issues/{1:d}'.format(strs[1], issueid)
        
        r   = rq.get(url)
        if r.status_code != 200:
            irc.reply('Sorry, invalid repo or issue ID specified!')
            sys.exit(-4)

        j   = r.json()
        iss_title  = j['title']
        iss_state  = j['state']

#       Extract just the date of creation to be less verbose
        iss_crdate = j['created_at'].split('T')[0]

        iss_cldate = j['closed_at']
#       If closed, extract just the date of closing
        if iss_cldate:
            iss_cldate = iss_cldate.split('T')[0]
    
        # Get assignee info
        iss_assign = j['assignee']
        iss_assign_user = iss_assign['login'] if iss_assign else None

        # Check for associated PR, and determine if issue#xyz is really pr#xyz
        cat_str = "issue"
        try:
            pr = j['pull_request']
        except:
            pr = None
        if pr:
            pr_url = pr['url']
            pr_num = int(pr_url.split('/')[-1])
            if pr_num == issueid:
                cat_str = "pull request"

        mess = ('Github project {:s} {:s}#{:d}: \"{:s}\", '
                'created on {:s}, '
                'status: {:s}'.format(strs[1], cat_str, issueid, iss_title, iss_crdate, iss_state))
        if iss_cldate:
            mess += ' on {:s}'.format(iss_cldate)
        elif iss_assign_user:
            mess += ', assigned to {:s}'.format(iss_assign_user)
        else:
            mess += ', unassigned'

#        wwwurl = 'https://github.com/{:s}/issues/{:d}'.format(strs[1], issueid)
        wwwurl = j['html_url']
        mess += ', ' + wwwurl
        irc.reply(mess)

#        irc.reply(mess + ' -- https://api.github.com/repos/01org/dleyna-server/issues/{:s}'.format(ghid))

    gh = wrap(gh, (['somethingWithoutSpaces']))

Class = GitHub
#g.gh(sys.argv[1])

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
