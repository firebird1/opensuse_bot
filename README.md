## IRC Bot plugins for an irc bot run in openSUSE channels

* Bugtracker - A plugin for parsing irc chat for bug report urls or shortcodes and retrieving information about the given bug (Original source can be found here: https://code.launchpad.net/~krytarik/+git/ubuntu-bots/+ref/master Thanks to them and the other contributors for their work on this plugin).

* BugSearch - A plugin for searching openSUSE's bugzilla instance, found at https://bugzilla.opensuse.org. 

* GitHub - A plugin that parses github issues/pull requests links and shortcodes and retrieves information about them. 

* SubmitRequests - A plugin for parsing openSUSE submit request, delete request, etc. links and shortcodes and retrieves information about them.

